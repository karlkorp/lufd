#!/bin/bash

COMMENT_LINE=$(seq -s "#" 80 | sed 's/[0-9]//g')
SYSLOG_DIR=./SD
UNIQUE_SERIAL_NUMBERS=./UNIQUE_SERIAL_NUMBERS.TXT
USB_LOG_DIR=/var/log
USB_LOG_FILE=./USB.LOG
USB_SERIAL_NUMBERS=./USB_SN.TXT

pre_clean() {
  if [ -d "$SYSLOG_DIR" ]
  then
    rm -rf "$SYSLOG_DIR"
  fi
  find . -maxdepth 1 -iname "*.LOG" -delete
  find . -maxdepth 1 -iname "*.TXT" -delete
}

post_clean() {
  if [ -d "$SYSLOG_DIR" ]
  then
    rm -rf "$SYSLOG_DIR"
  fi
  if [ -f "$USB_SERIAL_NUMBERS" ]
  then
    rm "$USB_SERIAL_NUMBERS"
  fi
}

generate_common_syslog_file() {
  mkdir -p "$SYSLOG_DIR"

  find "$USB_LOG_DIR" -maxdepth 1 -iname "syslog*" \
       -exec sudo cp {} "$SYSLOG_DIR" \;

  find "$SYSLOG_DIR" -maxdepth 1 -iname "*.gz" \
       -exec sudo gzip -d {} \;

  sudo cat "$SYSLOG_DIR"/* | tee "$USB_LOG_FILE"
}

parse_usb_log_file() {
  while read -r line
  do
    case $line in
      *"Product:"*)
        usb_event_m=$(echo "$line"   | cut -d' ' -f1                )
        usb_event_d=$(echo "$line"   | cut -d' ' -f2                )
        usb_event_t=$(echo "$line"   | cut -d' ' -f3                )
        device_name=$(echo "$line"   | cut -d':' -f6 | sed 's/^.//' ) ;;

      *"Manufacturer:"*)
        manufacturer=$(echo "$line"  | cut -d':' -f6 | sed 's/^.//' ) ;;

      *"SerialNumber:"*)
        serial_number=$(echo "$line" | cut -d':' -f6 | sed 's/^.//' ) ;;
    esac
    echo "DATE: $usb_event_m $usb_event_d $usb_event_t"
    echo "USB device name: $device_name"
    echo "Manufacturer: $manufacturer"
    echo "Serial number: $serial_number" && echo "$serial_number" >> "$USB_SERIAL_NUMBERS"
    echo "$COMMENT_LINE" && echo
  done < "$USB_LOG_FILE"
  sort -u "$USB_SERIAL_NUMBERS" > "$UNIQUE_SERIAL_NUMBERS"
}

if [ -f "$USB_LOG_DIR"/syslog ]
then
  clear && pre_clean
  generate_common_syslog_file
  parse_usb_log_file
  post_clean
else
  echo "There is no 'syslog' file"
fi
