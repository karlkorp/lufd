# lufd: list usb-flash device ever plugged to the system

## Getting started

Programs is designed to extract information about mounted (ever mounted to a computer) USB devices.
Programs works on most modern operating systems of the GNU/Linux family.

## Usage

1. $ <code>chmod +x lufd_{1, 2}.sh</code> (optional, if the file is not executable)
2. $ <code>source lufd_{1, 2}.sh</code> (run program)
