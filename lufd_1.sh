#!/bin/bash

COMMENT_LINE=$(seq -s "#" 80 | sed 's/[0-9]//g')
UNIQUE_SERIAL_NUMBERS=./UNIQUE_SERIAL_NUMBERS.TXT
USB_LOG_FILE=./USB.LOG
USB_SERIAL_NUMBERS=./USB_SN.TXT

clean() {
  if [ -d "$SYSLOG_DIR" ]
  then
    rm -rf "$SYSLOG_DIR"
  fi
  find . -maxdepth 1 -iname "*.LOG" -delete
  find . -maxdepth 1 -iname "*.TXT" -delete
}

parse_usb_log_file() {
  while read -r line
  do
    case $line in
      *"Product:"*)
        usb_event_m=$(echo "$line"   | cut -d' ' -f1                )
        usb_event_d=$(echo "$line"   | cut -d' ' -f2                )
        usb_event_t=$(echo "$line"   | cut -d' ' -f3                )
        device_name=$(echo "$line"   | cut -d':' -f6 | sed 's/^.//' ) ;;

      *"Manufacturer:"*)
        manufacturer=$(echo "$line"  | cut -d':' -f6 | sed 's/^.//' ) ;;

      *"SerialNumber:"*)
        serial_number=$(echo "$line" | cut -d':' -f6 | sed 's/^.//' ) ;;
    esac
    echo "DATE: $usb_event_m $usb_event_d $usb_event_t"
    echo "USB device name: $device_name"
    echo "Manufacturer: $manufacturer"
    echo "Serial number: $serial_number" && echo "$serial_number" >> "$USB_SERIAL_NUMBERS"
    echo "$COMMENT_LINE" && echo
  done < "$USB_LOG_FILE"
  sort -u "$USB_SERIAL_NUMBERS" > "$UNIQUE_SERIAL_NUMBERS"
}

if command -v sudo journalctl > /dev/null 2>&1
then
  clear && clean
  sudo journalctl | grep -i -A 4 "new usb device found" > $USB_LOG_FILE
  parse_usb_log_file
fi
